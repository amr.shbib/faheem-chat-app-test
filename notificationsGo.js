// import PubNub from 'pubnub';
// import { Platform } from 'react-native';
// import PushNotification from 'react-native-push-notification';

// export default () => {
//   const pubnub = new PubNub({
//     subscribeKey: 'sub-c-37c7193e-8432-11ec-9f2b-a2cedba671e8',
//     publishKey: 'pub-c-a1e5f84b-74b1-464b-8964-c12f492bb292',
//     // uuid: Platform.OS === 'ios' ? 'PJ-ios' : 'PJ-android',
//   });
//   setTimeout(() => pubnub.publish({ channel: `notifications`, message: { id: 1 } }), 2000);
//   PushNotification.configure({
//     // Called when Token is generated.
//     onRegister(token) {
//       console.log('TOKEN:', token);
//       if (token.os === 'ios') {
//         pubnub.push.addChannels(
//           {
//             channels: ['notifications'],
//             device: token.token,
//             pushGateway: 'apns',
//           },
//           (status) => {
//             if (status.error) {
//               console.log('Operation failed: ', status);
//             } else {
//               console.log('Operation done!');
//             }
//           }
//         );
//         // Send to iOS from debug console: {"pn_apns":{"aps":{"alert":"Hello World."}}}
//       } else if (token.os === 'android') {
//         pubnub.push.addChannels(
//           {
//             channels: ['notifications'],
//             device: token.token,
//             pushGateway: 'gcm', // apns, gcm, mpns
//           },
//           (status) => {
//             if (status.error) {
//               console.log('Operation failed: ', status);
//             } else {
//               console.log('Operation done!');
//             }
//           }
//         );
//         // Send to Android from debug console: {"pn_gcm":{"data":{"message":"Hello World."}}}
//       }
//     },
//     // Called when a remote or local notification is opened or received.
//     onNotification(notification) {
//       console.log('Alert:', notification);
//       // Do something with the notification.
//       // Required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
//       // notification.finish(PushNotificationIOS.FetchResult.NoData);
//     },
//     // ANDROID: GCM or FCM Sender ID
//     senderID: '172597554333',
//   });
// };
