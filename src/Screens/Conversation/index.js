import { usePubNub } from 'pubnub-react';
import React, { useLayoutEffect, useRef } from 'react';
import { Image, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Chat } from '~/Components/Basic';
import { BackIcon } from '~/Components/Custom/Navigation';
import AccountActions from '~/Libs/Redux/Actions/Account';
import { isEmpty } from '~/Utils/Helper';
import useStyles from './styles';

var listener;
const Conversation = ({ navigation, route: { params } }) => {
  const styles = useStyles();

  const chatRef = useRef();
  const pubnub = usePubNub();
  const dispatch = useDispatch();
  const userId = useSelector((state) => state.account.userData.id);
  const chatChannel = 'chat19',
    msgRec1 = 'messageReceipts_1',
    msgRec2 = 'messageReceipts_2';
  const myChannel = [chatChannel, msgRec1, msgRec2];
  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => <BackIcon onPress={() => dispatch(AccountActions.setUserData({ id: null }))} />,
      headerTitle: () => <Image style={styles.imageU} resizeMode="contain" source={userId == 1 ? require('~/Assets/Images/user1.png') : require('~/Assets/Images/user2.png')} />,
    }),
      getData();
    onListener();
    pubnub.setUUID(userId);
    pubnub.subscribe({ channels: myChannel });
    dispatch(AccountActions.setUserData({ id: userId }));
    return () => {
      pubnub.removeListener(listener);
      pubnub.unsubscribeAll();
    };
  }, []);
  const getData = () => {
    pubnub.fetchMessages({ channels: myChannel }, (status, response) => {
      var messages = response.channels[chatChannel].map(({ timetoken, message }) => {
        return { ...message, date: new Date(message.date), timetoken };
      });
      var lastSeenChannel = msgRec1;
      if (userId == 1) lastSeenChannel = msgRec2;
      chatRef.current.onInit({ messages, lastSeen: !isEmpty(response.channels[lastSeenChannel]) ? response.channels[lastSeenChannel][response.channels[lastSeenChannel].length - 1].timetoken : null });
      pubnub.publish({ channel: `messageReceipts_${userId}`, message: { id: userId } });
    });
  };
  const onListener = () => {
    listener = {
      message: ({ channel, timetoken, message }) => {
        if (channel == chatChannel) {
          chatRef.current.appendMessages([{ ...message, date: new Date(message.date), timetoken }]);
          pubnub.publish({ channel: `messageReceipts_${userId}`, message: { id: userId } });
          if (message.id == userId)
            pubnub.publish({
              channel: 'notifications',
              message: {
                pn_gcm: {
                  data: {
                    message: { message: message.value, from: userId },
                  },
                },
                pn_apns: {
                  aps: { alert: message.value, from: userId },
                },
              },
            });
        } else if (message.id !== userId) chatRef.current.setLastSeen(timetoken);
      },
    };
    pubnub.addListener(listener);
  };
  const onSend = (message) => pubnub.publish({ channel: chatChannel, ...message }, () => {});
  return (
    <View style={styles.root}>
      <Chat ref={chatRef} myId={userId} onSendMessage={onSend} />
    </View>
  );
};

export default Conversation;
