import { makeStyles } from '~/Libs/Styles';
export default useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: 'grey',
    flex: 1,
  },
  imageU: {
    width: 80,
    height: 80,
  },
}));
