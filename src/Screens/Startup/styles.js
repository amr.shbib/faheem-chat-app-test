import { makeStyles } from '~/Libs/Styles';
export default useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.primary,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textAppName: {
    color: theme.palette.secondary,
    fontSize: 35,
    fontWeight: 'bold',
    width: '50%',
    textAlign: 'center',
  },
}));
