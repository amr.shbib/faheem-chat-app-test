import React, { useEffect } from 'react';
import { View } from 'react-native';
import { useDispatch } from 'react-redux';
import { Text } from '~/Components/Basic';
import StartupActions from '~/Libs/Redux/Actions/Startup';
import useStyles from './styles';

const Startup = () => {
  const styles = useStyles();

  const dispatch = useDispatch();
  useEffect(() => {
    setTimeout(() => dispatch(StartupActions.startup()), 2000);
  }, []);
  return (
    <View style={styles.root}>
      <Text style={styles.textAppName}>Faheem Chat</Text>
    </View>
  );
};

export default Startup;
