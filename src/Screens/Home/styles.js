import { makeStyles } from '~/Libs/Styles';
export default useStyles = makeStyles((theme) => ({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.palette.grey[0],
  },
  button: {
    width: '80%',
    height: 120,
    backgroundColor: 'rgba(0,0,0,0.2)',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderStyle: 'dashed',
    borderWidth: 5,
    borderColor: theme.palette.secondary,
    marginTop: 50,
  },
  textUser: {
    color: theme.palette.primary,
    fontSize: 25,
  },
  imageBackground: {
    position: 'absolute',
    flex: 1,
  },
  imageU1: {
    width: '100%',
    height: '100%',
  },
  imageU2: {
    width: '60%',
    height: '60%',
  },
}));
