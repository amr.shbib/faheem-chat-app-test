import React from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import { Text } from '~/Components/Basic';
import useStyles from './styles';
import { useDispatch } from 'react-redux';
import AccountActions from '~/Libs/Redux/Actions/Account';

const Home = ({ navigation }) => {
  const styles = useStyles();
  const dispatch = useDispatch();

  return (
    <View style={styles.root}>
      <Image style={styles.imageBackground} source={require('~/Assets/Images/cbg.png')} />
      <TouchableOpacity onPress={() => dispatch(AccountActions.setUserData({ id: '1' }))} style={styles.button}>
        <Image style={styles.imageU1} resizeMode="contain" source={require('~/Assets/Images/user1.png')} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => dispatch(AccountActions.setUserData({ id: '2' }))} style={styles.button}>
        <Image style={styles.imageU2} resizeMode="contain" source={require('~/Assets/Images/user2.png')} />
      </TouchableOpacity>
    </View>
  );
};
export default Home;
