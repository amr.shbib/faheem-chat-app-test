import PubNub from 'pubnub';
import { PubNubProvider } from 'pubnub-react';
import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { SnackBar } from '~/Components/Basic';
import Constants from '~/Configs/Constants';
import init from '~/Configs/Init';
import { NavigationGenerator } from '~/Libs/Navigation';
import { persistor, store } from '~/Libs/Redux/Store';
import { NotificationProvider, ThemeProvider } from '~/Services/Providers';

const {
  pubnub: { subscribeKey, publishKey },
} = Constants;
export default App = () => {
  const pubnub = new PubNub({ subscribeKey, publishKey, uuid: Platform.OS === 'ios' ? 'PJ-ios' : 'PJ-android' });
  useEffect(() => {
    init({ pubnub });
  }, []);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NotificationProvider>
          <ThemeProvider>
            <PubNubProvider client={pubnub}>
              <NavigationGenerator />
              <SnackBar />
            </PubNubProvider>
          </ThemeProvider>
        </NotificationProvider>
      </PersistGate>
    </Provider>
  );
};
