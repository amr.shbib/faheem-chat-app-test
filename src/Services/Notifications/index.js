import Constants from '~/Configs/Constants';
import PushNotification from 'react-native-push-notification';

const {
  notification: { senderId },
} = Constants;

const initNotification = (pubnub) => {
  PushNotification.createChannel({ channelId: 'myNotifications', channelName: 'myNotifications' }, (created) => console.log(`CreateChannel returned '${created}'`));
  PushNotification.configure({
    onRegister(token) {
      console.log('TOKEN:', token);
      if (token.os === 'ios') {
        pubnub.push.addChannels({ channels: ['notifications'], device: token.token, pushGateway: 'apns' }, (status) => {
          if (status.error) console.log('Operation failed: ', status);
          else console.log('Operation done! IOS');
        });
        // {"pn_apns":{"aps":{"alert":"Hello World."}}}
      } else if (token.os === 'android') {
        pubnub.push.addChannels({ channels: ['notifications'], device: token.token, pushGateway: 'gcm' }, (status) => {
          if (status.error) console.log('Operation failed: ', status);
        });
        // {"pn_gcm":{"data":{"message":"Hello World."}}}
      }
    },
    onNotification(notification) {
      const data = JSON.parse(notification.data.message);
      var { store } = require('~/Libs/Redux/Store');
      var { id } = store.getState().account.userData;
      if (data.from !== id) PushNotification.localNotification({ channelId: 'myNotifications', title: 'New Message', message: data.message });
      console.log('Alert:', notification.data.message);
    },
    senderID: senderId,
    permissions: { alert: true, badge: true, sound: true },
    popInitialNotification: true,
    requestPermissions: true,
  });
};

export { initNotification };

// {
//   "pn_gcm": {
//     "data": { "message": "Hello World!" }
//   },
//   "pn_apns": {
//     "aps": { "alert": "Hello World!" }
//   },
//  "pn_debug": true
// }
