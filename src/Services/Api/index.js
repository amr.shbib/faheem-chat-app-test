import { create } from 'apisauce';
import axios from 'axios';
import Constant from '~/Configs/Constants';
import { beautifyParams, beautifyQueries } from '~/Utils/Api';
const { url, api, headers } = Constant;
const CancelToken = axios.CancelToken;
const requestsSourceCancellation = CancelToken.source();

const axiosInstance = axios.create({
  baseURL: url.api,
  timeout: api.timeout,
  headers: {
    Accept: `application/json;v=${api.version}`,
    accept: `application/json;v=${api.version}`,
    'api-version': api.version,
    cancelToken: requestsSourceCancellation.token,
    ...headers,
  },
});
const Api = create({ axiosInstance: axiosInstance });

const httpRequest = (method, url, { params, queries, body, headers: additionalHeaders = {} }) => {
  var { store } = require('~/Libs/Redux/Store');
  var { token } = store.getState().account.userData;
  var headers;
  var headers = token ? { headers: { authorization: token, ...additionalHeaders } } : { headers: additionalHeaders };
  var urlRequest = `${url}${beautifyParams(params)}${beautifyQueries(queries)}`;

  var methodRequest = {
    get: () => Api.get(urlRequest, {}, headers),
    post: () => Api.post(urlRequest, body, headers),
    delete: () => Api.delete(urlRequest, {}, headers),
    put: () => Api.put(urlRequest, body, headers),
  };
  return methodRequest[method]();
};

export { httpRequest };
