import React, { useState, createContext, useContext, useEffect } from 'react';
import SafeArea from 'react-native-safe-area';
import { darkTheme } from '~/Constants/Themes';

const THEME_INITIAL_STATE = darkTheme;

export const ThemeContext = createContext(THEME_INITIAL_STATE);

export function useTheme() {
  return useContext(ThemeContext);
}

export const ThemeConsumer = ThemeContext.Consumer;

export function Theme({ children }) {
  const [theme, setTheme] = useState(THEME_INITIAL_STATE);
  useEffect(() => {
    async function safeArea() {
      const { safeAreaInsets } = await SafeArea.getSafeAreaInsetsForRootView();
      top = safeAreaInsets.top;
      bottom = safeAreaInsets.bottom;
      setTheme({ ...darkTheme, safearea: { top, bottom } });
    }
    safeArea();
  }, []);

  return <ThemeContext.Provider value={{ theme, setTheme }}>{children}</ThemeContext.Provider>;
}
