import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, RefreshControl, Text, View, StyleSheet } from 'react-native';
import constants from '~/Configs/Constants';
import { beautifyQueries, beautifyParams } from '~/Utils/Api';
import { getAttrData } from '~/Utils/Helper';
const { url } = constants;
const RemoteData = (props) => {
  var {
    url = ``,
    pagination = true,
    initPage = 1,
    urlParams = null,
    urlQueries = null,
    data = [],
    dataAttr = [],
    onFetchData = () => {},
    renderEmpty = () => (
      <View style={styles.emptyContainer}>
        <Text>Empty</Text>
      </View>
    ),
  } = props;
  url = `${url.api}${url}`;
  const [fetchData, setFetchData] = useState(data);
  const [page, setPage] = useState(initPage);
  const [disableLoadMore, setDisableLoadMore] = useState(false);
  const [isRefresh, setIsRefresh] = useState(false);

  useEffect(() => {
    setPage(initPage);
  }, []);

  useEffect(() => {
    page > initPage && getData();
  }, [page]);

  useEffect(() => {
    Reset();
    getData(true);
  }, [urlQueries]);

  useEffect(() => {
    onFetchData(fetchData);
  }, [fetchData]);

  const getData = async (first = false) => {
    if (pagination) urlQueries = { ...urlQueries, page: first ? initPage : page };
    QueriesParams = beautifyParams(urlParams) + beautifyQueries(urlQueries);
    resp = await fetch(`${url}${QueriesParams}`);
    respJson = await resp.json();
    setFetchData(first ? [...getAttrData(dataAttr, respJson)] : [...fetchData, ...getAttrData(dataAttr, respJson)]);
    if (respJson.content.length == 0 || !pagination) setDisableLoadMore(true);
  };
  const onRefresh = () => {
    Reset();
    getData(true);
  };
  const Reset = () => {
    setFetchData([]);
    setPage(initPage);
    setDisableLoadMore(false);
    setIsRefresh(false);
  };

  var isLoading = fetchData.length == 0 && page == initPage && !disableLoadMore;
  var isEmpty = fetchData.length == 0 && disableLoadMore;

  if (isLoading) return <ActivityIndicator style={styles.loading} size={'small'} color="#999999" />;
  if (isEmpty) return renderEmpty();

  return (
    <FlatList
      contentContainerStyle={styles.listContainer}
      onEndReached={() => !disableLoadMore && setPage(page + 1)}
      onEndReachedThreshold={0.5}
      refreshControl={<RefreshControl refreshing={isRefresh} onRefresh={onRefresh} />}
      data={fetchData}
      keyExtractor={(item, index) => `RemoteData-item-${index}`}
      {...props}
    />
  );
};

const styles = StyleSheet.create({
  listContainer: {
    justifyContent: 'center',
  },
  loading: {
    flex: 1,
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

/*
  -------------------------------- Example --------------------------------
    <RemoteData
      url={"http:....."}
      pagination
      urlQueries={{ limit: 3 }}
      urlParams={{param1:'data1'}}
      initPage={1}
      dataAttr={["content"]}
      renderEmpty={() => (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
          <Text>Empty Props</Text>
        </View>
      )}
      renderItem={() => <View style={{ width: "100%", marginTop: 10, height: 50, backgroundColor: "red" }} />}
    /> 
*/
export default RemoteData;
