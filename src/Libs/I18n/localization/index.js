import i18n, { LanguageDetectorAsyncModule, Services, InitOptions } from 'i18next';
import { initReactI18next } from 'react-i18next';
import AsyncStorage from '@react-native-community/async-storage';
import * as RNLocalize from 'react-native-localize';
import Constant from '~/Configs/Constants';

import en from './en';
import ar from './ar';

export const AVAILABLE_LANGUAGES = {
  en,
  ar,
};

const AVALAILABLE_LANG_CODES = Object.keys(AVAILABLE_LANGUAGES);

const languageDetector = (setUserData) => ({
  type: 'languageDetector',
  // If this is set to true, your detect function receives a callback function that you should call with your language,
  //useful to retrieve your language stored in AsyncStorage for example
  async: true,
  init: (_services, _detectorOptions, _i18nextOptions) => {
    /* use services and options */
  },
  detect: (callback) => {
    AsyncStorage.getItem(Constant.storageKey.language, (err, lng) => {
      // Case 1: Error fetching stored data
      if (err || !lng) {
        if (err) {
        } else {
        }
        const bestLng = RNLocalize.findBestAvailableLanguage(AVALAILABLE_LANG_CODES);

        callback(bestLng?.languageTag ?? 'en');
        return;
      }
      callback(lng);
    });
  },
  cacheUserLanguage: (lng) => {
    AsyncStorage.setItem(Constant.storageKey.language, lng);
    setUserData({ lang: lng });
  },
});
export const setLanguage = (setUserData) => {
  i18n
    .use(languageDetector(setUserData))
    .use(initReactI18next)
    .init({
      resources: AVAILABLE_LANGUAGES,
      react: {
        useSuspense: false,
      },
      interpolation: {
        escapeValue: false,
      },
      defaultNS: 'common',
    });
};
