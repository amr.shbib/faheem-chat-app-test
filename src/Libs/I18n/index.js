import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import * as RNLocalize from 'react-native-localize';
import AsyncStorage from '@react-native-community/async-storage';

export default initLanguage = (Configs) => {
  return new Promise((resolve) => {
    const codes = Object.keys(Configs.resources);

    const LanguageDetector = {
      type: 'languageDetector',
      async: true,
      init: (_services, _detectorOptions, _i18nextOptions) => {},
      detect: (callback) => {
        //callback to setLanguage to i18
        AsyncStorage.getItem(Configs.storageKey, (err, lng) => {
          if (err || !lng) {
            const bestLng = RNLocalize.findBestAvailableLanguage(codes);
            callback(bestLng?.languageTag ?? Configs.default);
            return;
          }
          callback(lng);
        });
      },
      cacheUserLanguage: (lng) => AsyncStorage.setItem(Configs.storageKey, lng),
    };
    i18n
      .use(LanguageDetector)
      .use(initReactI18next)
      .init({
        resources: Configs.resources,
        react: {
          useSuspense: false,
        },
        interpolation: {
          escapeValue: false,
        },
        defaultNS: 'common',
      })
      .then(() => resolve(i18n.language));
  });
};
