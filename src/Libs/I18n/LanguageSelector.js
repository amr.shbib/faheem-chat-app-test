import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, I18nManager } from 'react-native';
import { useTranslation } from 'react-i18next';
import RNRestart from 'react-native-restart';
import Configs from '~/Configs/Language';

const LanguageSelector = ({ containerStyle, selectedStyle, style, selectedText, text }) => {
  const { t, i18n } = useTranslation();
  const selectedLanguage = i18n.language;

  const updateLanguage = async (lang) => {
    await i18n.changeLanguage(lang.value);

    if (lang.dir == 'rtl' && !I18nManager.isRTL) {
      I18nManager.forceRTL(true);
      RNRestart.Restart();
    }
    if (lang.dir == 'ltr' && I18nManager.isRTL) {
      I18nManager.forceRTL(false);
      RNRestart.Restart();
    }
  };
  return (
    <View style={[styles.container, containerStyle]}>
      {Configs.langs.map((lang) => {
        const selected = lang.value === selectedLanguage;
        return (
          <TouchableOpacity
            key={lang.value}
            style={selected ? selectedStyle : style}
            disabled={selected}
            onPress={() => updateLanguage(lang)}
          >
            <Text style={selected ? selectedText : text}>{lang.label}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default LanguageSelector;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
