import React from 'react';
import RNRestart from 'react-native-restart';

import { Alert } from 'react-native';

export default class ErrorsHandlerProvider extends React.Component {
  componentDidCatch(error, errorInfo) {
    Alert.alert(
      'Oh My Exception!',
      "oops...seems like we encountered an alien exception...we'll direct you to the home screen and you can continue browsing the app",
      [{ text: 'Go to home', onPress: this.restart }],
      { cancelable: false }
    );
  }

  restart = () => {
    RNRestart.Restart();
  };

  render() {
    return this.props.children;
  }
}
