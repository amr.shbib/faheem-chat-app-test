import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'remote-redux-devtools';
import { persistStore } from 'redux-persist';

export default (rootReducer, rootSaga) => {
  const middleware = [];
  const enhancers = [];
  // Saga
  const sagaMiddleware = createSagaMiddleware();
  middleware.push(sagaMiddleware);
  // Push middlewares
  enhancers.push(applyMiddleware(...middleware));
  var store = createStore(rootReducer, composeWithDevTools(...enhancers));
  // var store = createStore(rootReducer, compose(...enhancers, !!window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));
  sagaMiddleware.run(rootSaga);
  let persistor = persistStore(store);
  return { store, persistor };
};
