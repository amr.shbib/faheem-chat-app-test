import { put } from 'redux-saga/effects';
import EntityActions from '../Actions/Entity';

export default {
  *get(status, id, data) {
    switch (status) {
      case 200:
        yield put(EntityActions.getSucceeded(id, data));
        break;
      case 400:
        yield put(EntityActions.getFailed(id, { message: data.message || data.error_des }));
        break;
      default:
        yield put(EntityActions.getFailed(id, { message: data.message || data.error_des }));
    }
  },
  *post(status, id, data) {
    switch (status) {
      case 200:
        yield put(EntityActions.postSucceeded(id, data));
        break;
      case 400:
        yield put(EntityActions.postFailed(id, { message: data.message || data.error_des }));
        break;
      default:
        yield put(EntityActions.postFailed(id, { message: data.message || data.error_des }));
    }
  },
  *put(status, id, data) {
    switch (status) {
      case 200:
        yield put(EntityActions.putSucceeded(id, data));
        break;
      case 400:
        yield put(EntityActions.putFailed(id, data));
        break;
      default:
        yield put(EntityActions.putFailed(id, data));
    }
  },
  *delete(status, id, data) {
    switch (status) {
      case 200:
        yield put(EntityActions.deleteSucceeded(id, data));
        break;
      case 400:
        yield put(EntityActions.deleteFailed(id, data));
        break;
      default:
        yield put(EntityActions.deleteFailed(id, data));
    }
  },
};
