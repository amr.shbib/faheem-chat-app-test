import { put } from 'redux-saga/effects';
import StartupActions from '~/Libs/Redux/Actions/Startup';

export default {
  *startup() {
    yield put(StartupActions.onStartupSucceeded());
  },
};
