import { takeEvery, take, fork, takeLatest, all } from 'redux-saga/effects';

/* ------------- Types ------------- */
import { StartupTypes } from '~/Libs/Redux/Actions/Startup';
import { AccountTypes } from '~/Libs/Redux/Actions/Account';
import { EntityTypes } from '~/Libs/Redux/Actions/Entity';

/* ------------- Sagas ------------- */
import startupSagas from './Startup';
import accountSagas from './Account';
import entitySagas from './Entity';

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    takeLatest(StartupTypes.STARTUP, startupSagas.startup),
    takeLatest(AccountTypes.GET_PROFILE, accountSagas.getProfile),
    // takeLatestByStoreId(EntityTypes.POST, entitySagas.post),
    // takeLatestByStoreId(EntityTypes.GET, entitySagas.get),
    takeEvery(EntityTypes.POST, entitySagas.post),
    takeEvery(EntityTypes.GET, entitySagas.get),
  ]);
}

const takeLatestByStoreId = (patternOrChannel, saga, ...args) =>
  fork(function* () {
    let lastTasks = {};
    let storeId = '';
    while (true) {
      const action = yield take(patternOrChannel);
      if (storeId == action.id && action.id !== '/generalHandler') yield lastTasks[action.type].cancel();
      storeId = action.id;
      lastTasks[action.type] = yield fork(saga, ...args.concat(action));
    }
  });
