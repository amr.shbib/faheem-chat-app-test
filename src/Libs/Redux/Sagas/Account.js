import get from 'lodash/get';
import { call, put, select } from 'redux-saga/effects';
import AccountActions from '~/Libs/Redux/Actions/Account';

export default {
  *getProfile() {
    const token = yield select((store) => store.account.token);
    const res = yield call(api.account.get, {}, { headers: { authentication_token: token } });

    if (res.ok) {
      const profile = get(res, 'data', null);
      yield put(AccountActions.getProfileSucceeded(profile));
    } else {
      const errors = get(res, 'data.errors', []);
      yield put(AccountActions.getProfileFailed(errors));
    }
  },
};
