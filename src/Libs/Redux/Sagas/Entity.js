import { httpRequest } from '~/Services/Api';
import After from './Middleware';

const auth = ['/login'];
var res = {};
export default {
  *get({ apiUrl, data }) {
    try {
      res = yield httpRequest('get', apiUrl, data);
      yield After.get(200, apiUrl, res.data);
    } catch (err) {
      yield After.get(400, apiUrl, { message: 'Connection Error' });
    }
  },

  *post({ apiUrl, data }) {
    try {
      res = yield httpRequest('post', apiUrl, data);
      if (res.ok && auth.includes(apiUrl)) {
      }
      yield After.post(res.status, apiUrl, res.data);
    } catch (err) {
      yield After.post(400, apiUrl, { message: 'Connection Error' });
    }
  },
};
