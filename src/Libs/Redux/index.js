import storage from '@react-native-community/async-storage';
import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import Reducers from '~/Libs/Redux/Actions';
import rootSaga from '~/Libs/Redux/Sagas';
import configureStore from './CreateStore';

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['entity', 'startup'],
  stateReconciler: autoMergeLevel2, // merge with old redux if app updated
};

/* ------------- Assemble The Reducers ------------- */
export default () => {
  let reducers = combineReducers(Reducers);
  const persistedReducer = persistReducer(persistConfig, reducers);
  return configureStore(persistedReducer, rootSaga);
};
