import { reducer as entity } from './Entity';
import { reducer as account } from './Account';
import { reducer as startup } from './Startup';

export default {
  account,
  entity,
  startup,
};
