import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions(
  {
    startup: null,
    onStartupSucceeded: null,
  },
  {
    prefix: 'App/',
  }
);

export const StartupTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  loading: true,
});

/* ------------- Reducers ------------- */

export const onStartupSucceeded = (state) => state.merge({ loading: false });
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ON_STARTUP_SUCCEEDED]: onStartupSucceeded,
});
