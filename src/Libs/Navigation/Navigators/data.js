import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';

export const Navigators = {
  Tab: createBottomTabNavigator,
  Drawer: createDrawerNavigator,
  Stack: createStackNavigator,
};
