import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { useSelector } from 'react-redux';
import { isEmpty } from '~/Libs/Form/Utils/Helpers';
import { TabBar } from '../Utils/Generators';
import { Navigators } from './data';

/* -------------------------------- Navigators -------------------------------- */
const NavigatorsBuild = ({ activeNavigation }) => {
  if (!isEmpty(activeNavigation)) {
    if (activeNavigation.type == 'Screen') return activeNavigation.component();
    const { type, screens, propsNavigator } = activeNavigation;
    var NavTypeContainer = Navigators[type]();
    var updatedProps = {};
    if (type == 'Tab' && !!propsNavigator?.tabBarButton) updatedProps = { tabBar: (props) => <TabBar {...props} CustomBottomBar={propsNavigator.tabBarButton} /> };
    return (
      <NavTypeContainer.Navigator {...propsNavigator} {...updatedProps}>
        {screens.map(({ component, ...resProps }) => {
          return (
            <>
              {!!component?.type && <NavTypeContainer.Screen {...resProps}>{() => <NavigatorsBuild activeNavigation={component} />}</NavTypeContainer.Screen>}
              {!!!component?.type && <NavTypeContainer.Screen component={component} {...resProps} />}
            </>
          );
        })}
      </NavTypeContainer.Navigator>
    );
  } else return null;
};

export default NavigatorsBuild;
