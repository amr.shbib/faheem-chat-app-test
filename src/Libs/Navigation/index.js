// import useForm from './Utils';  TODO: useNavigation => for currentRoute  + navigationScreens:['home','login']
import setNavigationConfig from './Config';
import NavigationGenerator from './Builder';
import { navigationRef } from './Builder';
export { setNavigationConfig, NavigationGenerator, navigationRef };
