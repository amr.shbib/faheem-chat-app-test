import React from 'react';
import defaultConfig from './data';

export const navigationConfig = React.createRef();
export default setNavigationConfig = (config) => {
  navigationConfig.current = {
    ...defaultConfig,
    ...config,
  };
};
