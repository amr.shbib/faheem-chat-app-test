import React from 'react';
import { Platform, TouchableOpacity, View } from 'react-native';

/* -------------------------------- TabBar Style -------------------------------- */
export const TabBar = ({ state, descriptors, navigation, CustomBottomBar }) => {
  const styles = {
    tabBarRoot: {
      flexDirection: 'row',
      paddingBottom: Platform.OS == 'ios' ? 10 : 0,
      justifyContent: 'space-around',
      backgroundColor: 'white',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.22,
      shadowRadius: 4.22,
      elevation: 6,
    },
  };
  const focusedOptions = descriptors[state.routes[state.index].key].options;
  if (focusedOptions.tabBarVisible === false) return null;
  return (
    <View style={styles.tabBarRoot}>
      {state.routes.map((route, index) => {
        const {
          options: { tabBarLabel, title, icon },
        } = descriptors[route.key];
        const label = tabBarLabel !== undefined ? tabBarLabel : title !== undefined ? title : route.name;
        const isFocused = state.index === index;
        const onPress = () => {
          const event = navigation.emit({ type: 'tabPress', target: route.key, canPreventDefault: true });
          if (!isFocused && !event.defaultPrevented) navigation.navigate(route.name);
        };
        var buttonProps = { isFocused, label, icon };
        return (
          <TouchableOpacity onPress={onPress} key={index}>
            <CustomBottomBar {...buttonProps} />
          </TouchableOpacity>
        );
      })}
    </View>
  );
};
