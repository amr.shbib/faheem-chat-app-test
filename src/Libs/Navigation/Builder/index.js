import { useReduxDevToolsExtension } from '@react-navigation/devtools';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { useSelector } from 'react-redux';
import { isEmpty } from '~/Utils/Helper';
import { navigationConfig } from '../Config';
import NavigatorsBuild from '../Navigators';

export const navigationRef = React.createRef();

export default NavigationGenerator = () => {
  useReduxDevToolsExtension(navigationRef);
  const state = useSelector(
    (s) => s,
    (prev, next) => prev.startup === next.startup && prev.account === next.account
  );
  const getRootNavigation = (navigationsAll) => {
    if (!isEmpty(navigationsAll))
      for (var i = 0; i < navigationsAll.length; i++) {
        var canAccessToNavigation = navigationsAll[i]?.dependencies?.every((dependency) => dependency.isValid(eval(`state.${dependency.prop}`)));
        if (isEmpty(canAccessToNavigation) || canAccessToNavigation) {
          if (!!navigationsAll[i]?.screens) return { ...navigationsAll[i], screens: getFromScreen(navigationsAll[i].screens) };
        }
      }
  };
  const getFromScreen = (navScreens) => {
    var screens = [];
    for (var i = 0; i < navScreens.length; i++) {
      if (typeof navScreens[i].component == 'object') screens = [...screens, { ...navScreens[i], component: getRootNavigation(navScreens[i].component) }];
      else screens = [...screens, { ...navScreens[i], component: navScreens[i].component }];
    }
    return screens;
  };

  const { navigations } = navigationConfig.current;
  const theme = { colors: { backgroundColor: '#fff' } };
  return (
    <NavigationContainer ref={navigationRef} theme={theme}>
      <NavigatorsBuild activeNavigation={getRootNavigation(navigations)} />
    </NavigationContainer>
  );
};
