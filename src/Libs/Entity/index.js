import { useEffect } from 'react';
import EntityActions from '~/Libs/Redux/Actions/Entity';
import { useSelector, useDispatch } from 'react-redux';
import { useNotification } from '~/Services/Providers/Notification';
import PropTypes from 'prop-types';

function entityComponent({ storeId, onEntityReceived, onEntityPosted, onEntityUpdated, onEntityDeleted, onEntityReceivedError, onEntityPostedError, onEntityUpdatedError, onEntityDeletedError }) {
  const { setNotification } = useNotification();
  const dispatch = useDispatch();
  const entityStore = useSelector((state) => state.entity);
  const entity = {
    ...entityStore.byId[storeId],
    get: getRequest,
    post: postRequest,
    put: putRequest,
    delete: deleteRequest,
    unRegister,
  };
  useEffect(() => {
    !!storeId && register();
    // return (cleanUp = () => {
    //   unRegister();
    // });
  }, []);
  useEffect(() => {
    if (!!entityStore.byId[storeId]) {
      const { received, posted, deleted, updated, errors, responseFromGet, responseFromPost, responseFromPut, responseFromDelete } = entityStore.byId[storeId];

      if (received && !errors) {
        onEntityReceived && onEntityReceived(responseFromGet);
        resetProp('received');
      }
      if (posted && !errors) {
        onEntityPosted && onEntityPosted(responseFromPost);
        resetProp('posted');
      }
      if (updated && !errors) {
        onEntityUpdated && onEntityUpdated(responseFromPut);
        resetProp('updated');
      }
      if (deleted && !errors) {
        onEntityDeleted && onEntityDeleted(responseFromDelete);
        resetProp('deleted');
      }
      if (received && errors) {
        onEntityReceivedError && onEntityReceivedError(errors);
        setNotification({ open: true, type: 'error', body: errors.message });
        resetProp('received');
      }
      if (posted && errors) {
        onEntityPostedError && onEntityPostedError(errors);
        setNotification({ open: true, type: 'error', body: errors.message });
        resetProp('posted');
      }

      if (deleted && errors) {
        onEntityDeletedError && onEntityDeletedError(errors);
        setNotification({ open: true, type: 'error', body: errors.message });
        resetProp('deleted');
      }

      if (updated && errors) {
        onEntityUpdatedError && onEntityUpdatedError(errors);
        setNotification({ open: true, type: 'error', body: errors.message });
        resetProp('updated');
      }
    }
  }, [entityStore.byId[storeId]]);

  function register() {
    dispatch(EntityActions.register(storeId));
  }
  function unRegister() {
    dispatch(EntityActions.unRegister(storeId));
  }
  function getRequest(data) {
    dispatch(EntityActions.get(storeId, data));
  }

  function postRequest(data) {
    dispatch(EntityActions.post(storeId, data));
  }

  function putRequest(data) {
    dispatch(EntityActions.put(storeId, data));
  }

  function deleteRequest(data) {
    dispatch(EntityActions.delete(storeId, data));
  }
  function resetProp(prop) {
    dispatch(EntityActions.resetProp(storeId, prop));
  }
  return entity;
}
entityComponent.propTypes = {
  storeId: PropTypes.string.isRequired,
  onEntityReceived: PropTypes.func,
  onEntityPosted: PropTypes.func,
  onEntityUpdated: PropTypes.func,
  onEntityDeleted: PropTypes.func,
  onEntityReceivedError: PropTypes.func,
  onEntityPostedError: PropTypes.func,
  onEntityUpdatedError: PropTypes.func,
  onEntityDeletedError: PropTypes.func,
};

entityComponent.defaultProps = {
  storeId: null,
  onEntityReceived() {},
  onEntityPosted() {},
  onEntityUpdated() {},
  onEntityDeleted() {},
  onEntityReceivedError() {},
  onEntityPostedError() {},
  onEntityUpdatedError() {},
  onEntityDeletedError() {},
};
export default entityComponent;
