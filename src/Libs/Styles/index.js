import React from 'react';
import { useTheme } from '~/Services/Providers/Theme';
import { useTranslation } from 'react-i18next';

export const makeStyles = (Comp) => () => {
  const { theme } = useTheme();
  const {
    i18n: { language },
  } = useTranslation();
  return Comp(theme, language);
};

export const withStyles = (styles) => (Comp) =>
  React.forwardRef(({ children, ...props }, ref) => {
    const { theme } = useTheme();
    const {
      i18n: { language },
    } = useTranslation();
    return (
      <Comp ref={ref} {...props} styles={styles(theme, language)}>
        {children}
      </Comp>
    );
  });
