import React from 'react';
import { KeyboardAvoidingView, Platform, ScrollView } from 'react-native';

const FormWrapper = ({ children, offset, refrance, bounces = true }) => {
  return (
    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: 'white' }} behavior={Platform.OS == 'ios' ? 'padding' : null} keyboardVerticalOffset={offset || 90}>
      <ScrollView nested={false} ref={refrance} bounces={bounces} contentContainerStyle={{ paddingVertical: 10 }} showsVerticalScrollIndicator={false}>
        {children}
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default FormWrapper;
