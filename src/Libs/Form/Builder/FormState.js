import reduce from 'lodash/reduce';
import set from 'lodash/set';

import FieldState from './FieldState';

export default class Schema {
  constructor(formDefinition) {
    const formState = reduce(
      formDefinition,
      (form, options, fieldKey) => set(form, fieldKey, new FieldState({ form: formDefinition, ...options })),
      {}
    );

    return formState;
  }
}
