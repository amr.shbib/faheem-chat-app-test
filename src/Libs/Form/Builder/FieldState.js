export default class FieldState {
  constructor(args) {
    const { validateOn, form, ...restOptions } = args;
    return {
      showError: false,
      dependencies: [],
      validators: [],
      validateOn: [...(validateOn || []), 'value'],
      ...restOptions,
      props: {
        value: '',
        hidden: false,
        ...restOptions.props,
      },
    };
  }
}
