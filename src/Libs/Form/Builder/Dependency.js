import React, { useEffect, useRef, useState } from 'react';
import { View } from 'react-native';
import { formConfig } from '../Config';
import { differentObject, isEmpty } from '../Utils/Helpers';

const Dependency = (params) => {
  const { props, SelectedField, form, fieldKey, updateForm, fieldType, onTriggerDependency } = params;
  const [propsControl, setPropsControl] = useState(props);
  const [triggerFields, setTrigerFields] = useState();
  const firstExec = useRef(true);
  useEffect(() => {
    let newTrigerFields = {};
    Object.keys(form)?.map((key) => {
      form[key]?.dependencies?.map(({ triggers, setProps, firstTrigger, id }) => {
        triggers.map((item) => {
          if (item.fieldKey == fieldKey) {
            if (!isEmpty(newTrigerFields[item.prop]))
              newTrigerFields = {
                ...newTrigerFields,
                [item.prop]: [
                  ...newTrigerFields[item.prop],
                  {
                    firstTrigger: !isEmpty(firstTrigger) && !firstTrigger,
                    id,
                    changeFieldKey: key,
                    setProps: setProps ? setProps : (form) => onTriggerDependency(form, id),
                  },
                ],
              };
            else
              newTrigerFields = {
                ...newTrigerFields,
                [item.prop]: [{ id, changeFieldKey: key, setProps: setProps ? setProps : (form) => onTriggerDependency(form, id) }],
              };
          }
        });
      });
    });

    setTrigerFields(newTrigerFields);
  }, []);
  useEffect(() => {
    if (!isEmpty(triggerFields)) {
      if (firstExec.current) onDependency(Object.keys(triggerFields));
      else {
        let diff = differentObject(propsControl, props);
        onDependency(diff);
      }
    }
  }, [props, triggerFields]);

  const onDependency = (diff) => {
    const { discardTriggerProps } = formConfig.current;

    if (!isEmpty(diff) && Object.keys(diff).filter((prop) => !discardTriggerProps.includes(prop))) {
      setPropsControl(props);
      diff = diff.filter((prop) => !discardTriggerProps.includes(prop));
      var updatedFields = { ...form };
      if (!!triggerFields[diff]) {
        triggerFields[diff].map(({ changeFieldKey, setProps, firstTrigger, id }) => {
          if (firstExec.current) {
            if (!!firstTrigger) {
              var newProps = !!setProps && setProps(form);
              updatedFields[changeFieldKey]['props'] = {
                ...updatedFields[changeFieldKey]['props'],
                ...newProps,
              };
            }
          } else {
            var newProps = !!setProps && setProps(form);
            updatedFields[changeFieldKey]['props'] = {
              ...updatedFields[changeFieldKey]['props'],
              ...newProps,
            };
          }
        });
        firstExec.current = false;
        updateForm(updatedFields);
      }
    }
  };
  if (props.hidden) return null;
  return (
    <View>
      <SelectedField {...props} />
    </View>
  );
};

export default Dependency;
