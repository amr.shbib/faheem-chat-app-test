import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { isEmpty } from '~/Utils/Helper';
import { formConfig } from '../Config';
import { KeyboardWrapper } from '../Components';
import { getRemoteForm } from '../Utils/RemoteForm';
import DependencyControl from './Dependency';
import FieldState from './FieldState';
import FormState from './FormState';

export default useForm = (formData) => {
  const [form, setForm] = useState();

  /*-------------------------------- Helpers Methods --------------------------------*/
  useEffect(() => {
    if (typeof formData?.form?.source == 'string') setForm(new FormState(getRemoteForm(form)));
    else setForm(new FormState(formData?.form?.source));
  }, []);

  const FormGenerator = ({ fieldsKeys = [], keyboardWrapper, onTriggerDependency }) => {
    if (!isEmpty(form)) {
      var formFieldsRender = fieldsKeys.length > 0 ? Object.keys(form).filter((fieldKey) => fieldsKeys.includes(fieldKey)) : Object.keys(form).filter((fieldKey) => fieldKey);
      var Wrapper = keyboardWrapper ? KeyboardWrapper : View;
      return <Wrapper style={{ flex: 1 }}>{formFieldsRender.map((fieldKey) => Field({ fieldKey, onTriggerDependency }))}</Wrapper>;
    }
  };
  const resetFormData = () => {
    setForm(new FormState(formData?.form?.source));
  };
  const Field = ({ fieldKey, onTriggerDependency }) => {
    let { fields } = formConfig.current;
    fields = { ...fields, ...formData?.form?.additionalFields };
    if (!!form) {
      var commonProps = {
        ...form[fieldKey],
        fieldKey,
        form: form,
        updateForm,
        onTriggerDependency,
        SelectedField: fields[form[fieldKey].fieldType],

        props: {
          ...form[fieldKey].props,
          onChange: (newValue) => _onFieldChange(fieldKey, newValue),
          validatorMessage: _showValidatorMessage(fieldKey),
        },
      };
      return <DependencyControl {...commonProps} />;
    }
    return null;
  };
  const updateFieldsValues = (fieldsKeys) => {
    let updatedForm = {};
    Object.keys(fieldsKeys)?.map((fieldKey) => {
      if (!!form[fieldKey]) updatedForm = { ...updatedForm, [fieldKey]: { ...form[fieldKey], props: { ...form[fieldKey].props, value: fieldsKeys[fieldKey] } } };
    });
    let newForm = { ...form, ...updatedForm };
    setForm(newForm);
  };
  const getformValues = () => {
    var formValues = {};
    if (!isEmpty(form))
      Object.keys(form)?.map((fieldKey) => {
        formValues = { ...formValues, [fieldKey]: form[fieldKey].props.value };
      });
    return formValues;
  };
  const getformFieldsValues = (fieldsKeys) => {
    var formFields = fieldsKeys.length > 0 ? Object.keys(form).filter((fieldKey) => fieldsKeys.includes(fieldKey)) : [];
    let formValues = {};
    if (!isEmpty(form))
      formFields?.map((fieldKey) => {
        formValues = { ...formValues, [fieldKey]: form[fieldKey].props.value };
      });
    return formValues;
  };
  const setFormData = (newForm) => {
    setForm(new FormState(newForm));
  };
  const updateForm = (updatedFields) => setForm(updatedFields);
  const updateField = (fieldKey, updatedField) => setForm({ ...form, [fieldKey]: updatedField });
  const addFormFields = (fields) => {
    var newFormFields = {};
    Object.keys(fields).map((fieldKey) => {
      newFormFields = { ...newFormFields, [fieldKey]: new FieldState({ ...fields[fieldKey] }) };
    });
    setForm({ ...form, ...newFormFields });
  };
  const removeFormField = (fieldKey) => {
    delete form[fieldKey];
    setForm(form);
  };
  const removeFormFields = (fieldsKey = []) => fieldsKey.map((fieldKey) => removeFormField(fieldKey));

  const validateFields = (fieldsKeys) => {
    var isValid = isFieldsValid(fieldsKeys);
    if (!isValid) showFieldsErrors(fieldsKeys); // set showError true for all fields
    return isValid;
  };
  const isFieldsValid = (fieldsKeys) => {
    var specificFields = {};
    fieldsKeys.map((fieldKey) => (specificFields = { ...specificFields, [fieldKey]: form[fieldKey] }));
    return isFormValid(specificFields);
  };
  const isFormValid = (validateForm = form) => {
    var statusFields = Object.keys(validateForm).map((fieldKey) => getFieldErrors(fieldKey).length === 0);
    const reducer = (accumulator, currentValue) => accumulator && currentValue;
    return statusFields.reduce(reducer);
  };
  const showFieldsErrors = (fieldsKeys) => {
    let updatedForm = {};
    fieldsKeys.map((fieldKey) => {
      var isHidden = form[fieldKey].props.hidden;

      updatedForm = { ...updatedForm, [fieldKey]: { ...form[fieldKey], showError: !isHidden && true } };
    });
    let newForm = { ...form, ...updatedForm };
    setForm(newForm);
  };
  const hideFieldsErrors = (fieldsKeys) => {
    let updatedForm = {};
    fieldsKeys.map((fieldKey) => {
      updatedForm = { ...updatedForm, [fieldKey]: { ...form[fieldKey], showError: false } };
    });
    let newForm = { ...form, ...updatedForm };
    setForm(newForm);
  };
  const showFormErrors = () => {
    var newForm = {};
    Object.keys(form).map((fieldKey) => {
      var isHidden = form[fieldKey].props.hidden;
      newForm = { ...newForm, [fieldKey]: { ...form[fieldKey], showError: !isHidden && true } };
    });
    setForm(newForm);
  };
  const hideFormErrors = () => {
    var newForm = {};
    Object.keys(form).map((fieldKey) => {
      newForm = { ...newForm, [fieldKey]: { ...form[fieldKey], showError: false } };
    });
    setForm(newForm);
  };

  const getFieldErrors = (fieldKey) => {
    const field = form[fieldKey];
    const {
      validators,
      props: { value },
    } = field;
    const errors = [];
    if (!field.validators) return errors;
    validators.map((validator) => {
      const validationResult = validator.validate(value, field, form);
      if (validationResult.valid === false) errors.push(validationResult);
    });
    return errors;
  };

  const formValues = () =>
    Object?.keys(form)?.map((fieldKey) => {
      return { [fieldKey]: form[fieldKey].props.value };
    });

  /*-------------------------------- Private Methods --------------------------------*/
  const _onFieldChange = (fieldKey, newValue, ...restParams) => {
    var newForm = {
      ...form,
      [fieldKey]: { ...form[fieldKey], props: { ...form[fieldKey].props, value: newValue } },
    };
    setForm(newForm);
  };
  const _showValidatorMessage = (fieldKey) => {
    const field = form[fieldKey];
    const { validatorMessage } = field;
    const errors = getFieldErrors(fieldKey);
    if (field.showError && errors.length) return errors[0].message;
    return validatorMessage;
  };
  return {
    FormGenerator,
    Field,
    formValues,
    isFormValid,
    showFormErrors,
    hideFormErrors,
    showFieldsErrors,
    hideFieldsErrors,
    validateFields,
    isFieldsValid,
    getFieldErrors,
    addFormFields,
    removeFormFields,
    removeFormField,
    updateField,
    updateForm,
    getformValues,
    getformFieldsValues,
    setFormData,
    updateFieldsValues,
    resetFormData,
  };
};

/*
1. fisrt call field => form==undefined

*/
