export default defaultConfig = {
  fields: {},
  discardTriggerProps: ['onChange', 'validatorMessage'],
  validatorsMessage: {
    isRequired: 'This field is required',
    minLength: 'Min length: ',
    maxLength: 'Max length: ',
    exactLength: 'Length must be equal: ',
    isNumber: 'This field must be a number',
    isMatches: 'Not equal: ',
    isEmailAddress: 'Invalid email format',
    isMobileNumber: 'Invalid phone number',
  },
};
