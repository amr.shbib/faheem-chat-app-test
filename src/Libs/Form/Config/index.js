import React from 'react';
import defaultConfig from './data';

export const formConfig = React.createRef();
export default setFormBuilderConfig = (config) => {
  formConfig.current = {
    ...defaultConfig,
    ...config,
    validatorsMessage: {
      ...defaultConfig.validatorsMessage,
      ...config.validatorsMessage,
    },
  };
};
