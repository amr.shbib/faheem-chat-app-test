import isEqual from 'lodash/isEqual';
import reduce from 'lodash/reduce';

export const isEmpty = (value) =>
  value === undefined ||
  value === null ||
  (typeof value === 'object' && Object.keys(value).length === 0) ||
  (typeof value === 'string' && value.trim().length === 0);

export const compareObjects = (firstVal, secondVal) => JSON.stringify(firstVal) === JSON.stringify(secondVal);
// export const differentObject = (obj1, obj2) => {
//   const result = {};
//   if (Object.is(obj1, obj2)) return undefined;

//   if (!obj2 || typeof obj2 !== 'object') return obj2;

//   Object.keys(obj1 || {})
//     .concat(Object.keys(obj2 || {}))
//     .forEach((key) => {
//       if (typeof obj1[key] !== 'function' && typeof obj2[key] !== 'function') {
//         if (obj2[key] !== obj1[key] && !Object.is(obj1[key], obj2[key])) result[key] = obj2[key];
//         if (typeof obj2[key] === 'object' && typeof obj1[key] === 'object') {
//           const value = diff(obj1[key], obj2[key]);
//           if (value !== undefined) result[key] = value;
//         }
//       }
//     });
//   return result;
// };

export const differentObject = (obj1, obj2) => {
  return reduce(
    obj1,
    function (result, value, key) {
      return isEqual(value, obj2[key]) ? result : result.concat(key);
    },
    []
  );
};
export const reducerOrAnd = (arr) => {
  const reducerOr = (accumulator, currentValue) => accumulator || currentValue;
  const reducerAnd = (accumulator, currentValue) => accumulator && currentValue;
  return arr.reduce(reducerOr).reduce(reducerAnd);
};

export const setPropertyValue = (obj, value, path) => {
  var i;
  path = path.split('.');
  for (i = 0; i < path.length - 1; i++) obj = obj[path[i]];
  obj[path[i]] = value;
};
