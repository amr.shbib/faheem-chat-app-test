import { useValidators } from '../';
export const getRemoteForm = (url) => {
  const { isRequired, minLength, maxLength, exactLength, isNumber, isMatches, isEmailAddress, isMobileNumber } = useValidators;

  //   resp = await fetch(url);
  //   respJson = await resp.json();
  var remoteForm = data;
  Object.keys(data).map((keyField) => {
    remoteForm = {
      ...remoteForm,
      [keyField]: {
        ...remoteForm[keyField],
        validators: remoteForm[keyField].validators.map((validator) => eval(validator)),
      },
    };
  });
  return remoteForm;
};

var data = {
  Country: {
    fieldType: 'TextInput',
    validators: ['isRequired()', 'minLength(20)'],
    props: {
      label: 'Country',
    },
    dependecies: [],
  },
};
