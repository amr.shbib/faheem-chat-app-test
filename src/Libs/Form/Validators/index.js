import isNaN from 'lodash/isNaN';
import Validator from './Validator';
import { parsePhoneNumberFromString as parseMobile } from 'libphonenumber-js/max';
import { formConfig } from '../Config';

const isRequired = (message) => {
  const { isRequired } = formConfig.current.validatorsMessage;
  message = !!message ? message : isRequired;
  return new Validator({
    key: 'REQUIRED',
    message,
    validator: (value) => (value === null || value === undefined || isNaN(value) ? false : `${value}`.trim().length > 0),
  });
};
const minLength = (length, message) => {
  const { minLength } = formConfig.current.validatorsMessage;
  message = !!message ? message : minLength;
  return new Validator({
    key: 'MIN_LENGTH',
    message: message + length,
    validator: (value) => `${value}`.length >= length,
  });
};

const maxLength = (length, message) => {
  const { maxLength } = formConfig.current.validatorsMessage;
  message = !!message ? message : maxLength;
  return new Validator({
    key: 'MAX_LENGTH',
    message: message + length,
    validator: (value) => `${value}`.length <= length,
  });
};

const exactLength = (length, message) => {
  const { exactLength } = formConfig.current.validatorsMessage;
  message = !!message ? message : exactLength;
  return new Validator({
    key: 'EXACT_LENGTH',
    message: message + length,
    validator: (value) => `${value}`.length === length,
  });
};

const isNumber = (message) => {
  const { isNumber } = formConfig.current.validatorsMessage;
  message = !!message ? message : isNumber;
  return new Validator({
    key: 'ONLY_NUMBERS',
    message,
    validator: (value) => /^\d*$/gi.test(`${value}`),
  });
};

const isMatches = (otherValue, message) => {
  const { isMatches } = formConfig.current.validatorsMessage;
  message = !!message ? message : isMatches;
  return new Validator({
    key: 'MATCHES',
    message,
    validator: (value) => value === otherValue(),
  });
};

const isEmailAddress = (message) => {
  const { isEmailAddress } = formConfig.current.validatorsMessage;
  message = !!message ? message : isEmailAddress;
  return new Validator({
    key: 'EMAIL',
    message,
    validator: (value) => /^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,64}$/gi.test(`${value?.trim()}`),
  });
};

const isMobileNumber = (message) => {
  const { isMobileNumber } = formConfig.current.validatorsMessage;
  message = !!message ? message : isMobileNumber;
  return new Validator({
    key: 'VALID_PHONE_NUMBER',
    message,
    validator: (value) => !!value && !!parseMobile(value?.trim())?.isValid(),
  });
};

export default {
  isRequired,
  minLength,
  maxLength,
  exactLength,
  isNumber,
  isMatches,
  isEmailAddress,
  isMobileNumber,
};
