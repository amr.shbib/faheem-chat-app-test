export default class Validator {
  constructor({ key, message, validator }) {
    this.key = key;
    this.message = message;
    this.validator = validator;
  }

  validate(value, field, form) {
    const { key, message } = this;
    const validation = this.validator(value, field, form);
    let valid = validation;

    return {
      key,
      message,
      valid,
    };
  }
}
