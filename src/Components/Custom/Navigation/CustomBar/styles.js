export default useStyles = {
  iconWrapper: {
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconTab: {
    margin: 17,
  },
  iconColor: (isFocused) => ({
    color: !isFocused ? 'red' : 'green',
  }),
};
