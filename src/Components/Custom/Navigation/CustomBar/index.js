import React from 'react';
import { View } from 'react-native';
import { Icon } from '~/Components/Basic';
import styles from './styles';
const CustomBar = (props) => {
  const {
    isFocused,
    icon: { component, name, size = 22, style, containerStyle, color },
  } = props;
  const iconColor = color ? { color } : {};
  return (
    <View style={[styles.iconWrapper, isFocused ? { backgroundColor: 'red' } : { backgroundColor: 'blue' }]}>
      {!!component ? component() : <Icon containerStyle={containerStyle} name={name} size={size} style={styles.iconTab} {...styles.iconColor(isFocused)} {...iconColor} />}
    </View>
  );
};

export default CustomBar;
