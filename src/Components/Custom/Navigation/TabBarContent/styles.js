import { makeStyles } from '~/Libs/Styles';
export default useStyles = makeStyles((theme) => ({
  tabBarRoot: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'white',
    paddingTop: theme.safearea.bottom / 4,
    paddingBottom: theme.safearea.bottom / 4,
  },
  stack: {
    flex: 1,
    shadowColor: '#FFF',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 5,
  },

  iconWrapper: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconTab: {
    marginBottom: 15,
  },
  iconColor: (isFocused) => ({
    color: !isFocused ? theme.palette.primary : theme.palette.secondary,
  }),
}));
