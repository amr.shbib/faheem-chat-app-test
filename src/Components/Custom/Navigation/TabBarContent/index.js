import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Icon } from '~/Components/Basic';
import useStyles from './styles';

/* -------------------------------- TabBar Style -------------------------------- */
export default ({ state, descriptors, navigation }) => {
  const styles = useStyles();
  const focusedOptions = descriptors[state.routes[state.index].key].options;
  if (focusedOptions.tabBarVisible === false) return null;
  return (
    <View style={styles.tabBarRoot}>
      {state.routes.map((route, index) => {
        const {
          options: { tabBarLabel, title, icon },
        } = descriptors[route.key];
        const label = tabBarLabel !== undefined ? tabBarLabel : title !== undefined ? title : route.name;
        const isFocused = state.index === index;
        const onPress = () => {
          const event = navigation.emit({ type: 'tabPress', target: route.key, canPreventDefault: true });
          if (!isFocused && !event.defaultPrevented) navigation.navigate(route.name);
        };
        return (
          <TouchableOpacity onPress={onPress} key={index}>
            <View style={styles.iconWrapper}>
              <Icon name={icon} size={icon == 'searchTab' ? 26 : 22} style={styles.iconTab} {...styles.iconColor(isFocused)} />
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};
