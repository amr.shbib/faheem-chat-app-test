import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { Pressable } from 'react-native';
import { Icon } from '~/Components/Basic';
import styles from './styles';

export const BackIcon = ({ color, style, onPress }) => {
  const navigation = useNavigation();
  return (
    <Pressable onPress={() => (onPress ? onPress() : navigation.goBack())} style={style}>
      <Icon containerStyle={styles.icon} size={25} name="back" color={color || styles.iconColor} />
    </Pressable>
  );
};

export const DrawerIcon = ({ color }) => {
  const navigation = useNavigation();
  return (
    <Pressable onPress={() => navigation.toggleDrawer()}>
      <Icon containerStyle={styles.icon} size={22} name="drawer" color={color || styles.iconColor} />
    </Pressable>
  );
};
