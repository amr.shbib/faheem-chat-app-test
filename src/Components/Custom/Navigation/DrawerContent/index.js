import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { connect, useSelector } from 'react-redux';
import { Icon, TextView } from '~/Components/Basic';
import AccountActions from '~/Libs/Redux/Actions/Account';
import { auth, unauth } from './data';
import useStyles from './styles';

function DrawerContent({
  navigation,
  logout,
  account: {
    userData: { token },
  },
}) {
  const styles = useStyles();
  const { profileData } = useSelector((state) => state.account.userData);

  let data = !!token ? auth : unauth;

  function navigate(screen) {
    if (screen == 'Login') navigation.navigate(screen, { redirect: 'Home' });
    if (screen == 'Logout') {
      logout();
      navigation.navigate('Home', { screen: 'Home' });
    } else navigation.navigate(screen);
  }
  return (
    <View style={styles.root}>
      {data.map(({ label, value, icon }) => (
        <>
          {value == 'Separator' ? (
            <View style={styles.sperator} />
          ) : (
            <TouchableOpacity activeOpacity={0.8} style={styles.item} onPress={() => navigate(value)}>
              <Icon size={20} color={'#034757'} name={icon} />
              <Text style={{ color: '#034757', marginLeft: 10 }}>{label}</Text>
            </TouchableOpacity>
          )}
        </>
      ))}
    </View>
  );
}

const mapStateToProps = (store) => ({
  account: store.account,
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(AccountActions.logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent);
