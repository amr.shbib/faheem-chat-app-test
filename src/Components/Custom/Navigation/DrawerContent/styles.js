import { makeStyles } from '~/Libs/Styles';
export default useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.safearea.top + Platform.OS == 'ios' ? 15 : 40,
    flex: 1,
  },
  item: {
    padding: 18,
    flexDirection: 'row',
  },
  sperator: {
    backgroundColor: theme.palette.grey[0],
    width: '100%',
    padding: 1,
  },
  image: { width: 77, height: 77, borderRadius: 77 / 2 },
}));
