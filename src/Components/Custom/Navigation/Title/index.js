import React from 'react';
import { Text } from '~/Components/Basic';
import useStyles from './styles';

const Title = ({ children, style }) => {
  const styles = useStyles();

  return <Text style={[styles.title, style]}>{children}</Text>;
};

export default Title;
