import React from 'react';
import { useTranslation } from 'react-i18next';
import { Text as Txt } from 'react-native';

const Text = ({ children, translate = false, ...rest }) => {
  const { t } = useTranslation();

  return (
    <Txt {...rest}>
      {!!translate && t(children)}
      {!!!translate && children}
    </Txt>
  );
};

export default Text;
