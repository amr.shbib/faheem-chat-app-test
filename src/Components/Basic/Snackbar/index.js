import React, { useState, useEffect, useMemo, useRef } from 'react';
import { View, Pressable, Animated } from 'react-native';
import { useNotification } from '~/Services/Providers/Notification';
import { Icon, TextView } from '~/Components/Basic';
import useStyles from './styles';

const SnackBar = () => {
  const { Notification, setNotification } = useNotification();
  const [id, setId] = useState(null);
  const opacity = useRef(new Animated.Value(0)).current;

  const NotificationContext = useMemo(
    () => ({
      open: Notification.open,
      type: Notification.type,
      body: Notification.body,
    }),
    [Notification]
  );
  const fadeIn = Animated.timing(opacity, {
    toValue: 1,
    duration: 350,
    usNativeDriver: true,
  });
  const fadeOut = Animated.timing(opacity, {
    toValue: 0,
    duration: 350,
    usNativeDriver: true,
  });

  useEffect(() => {
    if (!!NotificationContext.open) {
      clearTimeout(id);
      fadeIn.start(() => timeout());
    }
  }, [NotificationContext]);

  function timeout() {
    timeoutVar = setTimeout(() => {
      fadeOut.start(() => setNotification({ open: false }));
    }, 2500);
    setId(timeoutVar);
  }
  function handleClose() {
    id && clearTimeout(id);
    fadeOut.start(() => setNotification({ open: false }));
  }
  const { open, body, type } = NotificationContext;
  const styles = useStyles();

  if (!open) return null;

  return (
    <Animated.View style={[styles.container, styles.theme[type], { opacity }]}>
      <Pressable style={styles.press} onPress={handleClose}>
        <View style={styles.content}>
          <Text style={styles.text}>{body}</Text>
        </View>
      </Pressable>
    </Animated.View>
  );
};
export default SnackBar;
