import { makeStyles } from '~/Libs/Styles';

export default useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: 'transparent',
    height: 60,
    position: 'absolute',
    left: '5%',
    right: '5%',
    top: theme.safearea.top + 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    zIndex: 1000,
  },
  press: {
    width: '100%',
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  theme: {
    success: {
      backgroundColor: theme.palette.primary,
    },
    error: {
      backgroundColor: theme.palette.negative,
    },
    info: {
      backgroundColor: theme.palette.info,
    },
    warning: {
      backgroundColor: theme.palette.warning,
    },
  },
  content: {
    width: '75%',
  },
  text: {
    color: '#fff',
    marginLeft: 5,
  },
  icon: { width: 23, margin: 5 },
}));
