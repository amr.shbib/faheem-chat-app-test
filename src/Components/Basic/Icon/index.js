import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import Icons from '~/Constants/Icons';
const Icon = ({ name, style, containerStyle, width, height, size, color, onPress }) => {
  IconContainer = Icons[name];
  const Container = onPress ? TouchableOpacity : View;
  return (
    <View style={containerStyle}>
      <Container onPress={onPress} style={style}>
        {IconContainer && <IconContainer fill={color} width={size ? size : width} height={size ? size : height} style={{ color }} />}
      </Container>
    </View>
  );
};

export default Icon;
