import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import styles from './styles';

export default ({ children, style, loading }) => {
  return (
    <View style={style}>
      {!!loading && (
        <View style={styles.root}>
          <ActivityIndicator color="#999999" style={styles.loading} size={'large'} />
        </View>
      )}
      {!!!loading && children}
    </View>
  );
};
