export default {
  root: {
    height: '100%',
    justifyContent: 'center',
  },
  loading: {
    alignSelf: 'center',
  },
};
