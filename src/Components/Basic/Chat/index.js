import React, { forwardRef, useImperativeHandle, useRef, useState } from 'react';
import { FlatList, Image, TouchableOpacity, View } from 'react-native';
import { Text, TextInput, KeyboardWrapper } from '~/Components/Basic';
import { formatAMPM } from '~/Utils/Helper';
import useStyles from './styles';

const initState = { messageInput: '', messages: [], lastSeen: null };

const Chat = forwardRef((props, ref) => {
  const { myId = 1, onSendMessage = () => {} } = props;
  const styles = useStyles();
  chatRef = useRef();
  const [state, setStateData] = useState(initState);
  const setState = (newState) => setStateData({ ...state, ...newState });

  useImperativeHandle(ref, () => ({
    onInit({ messages, lastSeen }) {
      setState({ messages, lastSeen });
      setTimeout(() => chatRef?.current?.scrollToEnd(), 800);
    },
    setLastSeen(newLastSeen) {
      setState({ lastSeen: newLastSeen });
      setTimeout(() => chatRef?.current?.scrollToEnd(), 300);
    },
    appendMessages(newMessages) {
      setState({ messages: [...state.messages, ...newMessages], messageInput: '' });
      setTimeout(() => chatRef?.current?.scrollToEnd(), 300);
    },
  }));
  const renderMessage = ({ item }) => {
    const { id, value, date, timetoken } = item;
    const isMyMessage = id == myId;
    return (
      <View style={styles.containerMessage(isMyMessage)}>
        <Text style={styles.textValue}>{value}</Text>
        <View style={styles.textMoreDetails}>
          <Text style={styles.textDate(isMyMessage)}>{formatAMPM(date)}</Text>
          {id == myId && (
            <View style={styles.containerSeen}>
              {state.lastSeen >= timetoken && <Image style={styles.imageTick} source={require('~/Assets/Images/tick.png')} />}
              <Image style={styles.imageTick1} source={require('~/Assets/Images/tick.png')} />
            </View>
          )}
        </View>
      </View>
    );
  };
  const onSend = () => onSendMessage({ message: { id: myId, value: state.messageInput, date: new Date() } });
  const renderInputMessage = () => (
    <View style={styles.inputMessageContainer}>
      <TextInput placeholder={'Write a message...'} placeholderTextColor="rgba(255, 255, 255, 0.5)" value={state.messageInput} onChange={(messageInput) => setState({ messageInput })} style={styles.input} />
      <TouchableOpacity onPress={onSend} style={styles.iconOnSend}>
        <Image style={{ width: 18, height: 18, tintColor: 'white' }} source={require('~/Assets/Images/send.png')} />
      </TouchableOpacity>
    </View>
  );
  return (
    <View style={styles.root}>
      <KeyboardWrapper>
        <Image style={{ position: 'absolute', flex: 1 }} source={require('~/Assets/Images/cbg.png')} />
        <FlatList removeClippedSubviews={true} showsVerticalScrollIndicator={false} ref={(ref) => (chatRef.current = ref)} style={styles.messagesContainer} data={state.messages} renderItem={renderMessage} />
        {renderInputMessage()}
      </KeyboardWrapper>
    </View>
  );
});

export default Chat;
