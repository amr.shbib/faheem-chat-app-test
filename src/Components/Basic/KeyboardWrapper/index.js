import React, { Fragment } from 'react';
import { KeyboardAvoidingView, ScrollView, Platform } from 'react-native';

const KeyboardWrapper = ({ children, scrollable = false }) => {
  const ScrollableComponent = scrollable ? ScrollView : Fragment;
  return (
    <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} enabled style={{ flex: 1 }} keyboardVerticalOffset={90}>
      {children}
    </KeyboardAvoidingView>
  );
};

export default KeyboardWrapper;
