import React, { forwardRef, useRef, useState, useImperativeHandle } from 'react';
import { FlatList, Dimensions, View } from 'react-native';
let { width } = Dimensions.get('window');

export default forwardRef(({ data, renderItem }, ref) => {
  const onViewableItemsChanged = useRef(({ viewableItems }) => setActiveStep(viewableItems[0]?.index));
  const viewabilityConfig = useRef({ viewAreaCoveragePercentThreshold: 50 });
  const [activeStep, setActiveStep] = useState(0);
  const slider = useRef();

  useImperativeHandle(ref, () => ({
    scrollTo(index) {
      setActiveStep(index);
      slider.current.scrollToIndex({ index: index });
    },
  }));
  const onBack = () => {
    setActiveStep(activeStep - 1);
    slider.current.scrollToIndex({ index: activeStep - 1 });
  };
  const onNext = () => {
    setActiveStep(activeStep + 1);
    slider.current.scrollToIndex({ index: activeStep + 1 });
  };

  const props = {
    onBack,
    onNext,
    activeStep,

    item: {
      ...data[activeStep],
      component: () => (
        <FlatList
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          data={data}
          ref={(ref) => (slider.current = ref)}
          scrollEnabled={false}
          renderItem={({ item: { component }, index }) => {
            if (index !== activeStep) return <View style={{ width: width }} />;
            else return <View style={{ width: width, paddingHorizontal: 20 }}>{component()}</View>;
          }}
          onViewableItemsChanged={onViewableItemsChanged.current}
          viewabilityConfig={viewabilityConfig.current}
        />
      ),
    },
  };
  return renderItem(props);
});
