import React from 'react';
import { TextInput as TI } from 'react-native';

const TextInput = (props) => {
  const { validatorMessage, label, value, onChange, ...rest } = props;

  return <TI value={value} onChangeText={(text) => !!onChange && onChange(text)} {...rest} />;
};

export default TextInput;
