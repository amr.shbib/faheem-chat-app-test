import { Dimensions } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';

let { height, width } = Dimensions.get('window');

export const darkTheme = {
  palette: {
    primary: '#F79F2D',
    secondary: '#5C5B5B',
    tertiary: '#18b2aa',
    neon: '#c9fb36',
    grey: {
      0: '#F2F2F2',
      1: '#d5d5d5',
      2: '#a5a5a5',
      3: '#888888',
      4: '#4c4c4c',
    },
    bg: '#F8F9F9',
    white: '#ffffff',
    black: '#000000',
    transparent: '#ffffff00',
    facebook: '#4492FF',
    google: '#4285F4',
    positive: '#6CD79B',
    negative: '#d9534f',
    info: '#5bc0de',
    warning: '#f0ad4e',
  },
  font: {
    family: {
      main: 'Montserrat-Regular',
      semi: 'Montserrat-SemiBold',
      bold: 'Montserrat-Bold',
    },
    size: {
      s8: RFValue(8, 812),
      s10: RFValue(10, 812),
      s12: RFValue(12, 812),
      s14: RFValue(14, 812),
      s16: RFValue(16, 812),
      s18: RFValue(18, 812),
      s20: RFValue(20, 812),
      s22: RFValue(22, 812),
      s24: RFValue(24, 812),
      s26: RFValue(26, 812),
      s28: RFValue(28, 812),
      s30: RFValue(30, 812),
      s32: RFValue(32, 812),
    },
  },
  shadow: {
    0: {
      shadowColor: '#4c4c4c',
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: 3,
    },
    1: {
      shadowColor: '#a5a5a5',
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.18,
      shadowRadius: 1.0,
      elevation: 1,
    },
  },
  safearea: { top: 0, bottom: 0 },
  dimensions: { width, height },
};
