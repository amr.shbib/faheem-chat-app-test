import back from '~/Assets/Svg/back';
import drawer from '~/Assets/Svg/drawer';
import error from '~/Assets/Svg/error';
import home from '~/Assets/Svg/home';
import info from '~/Assets/Svg/info.svg';
import success from '~/Assets/Svg/success.svg';
import warning from '~/Assets/Svg/warning.svg';

export default {
  back,
  drawer,
  success,
  error,
  warning,
  info,
  home,
};
