export const isEmpty = (value) => value === undefined || value === null || (typeof value === 'object' && Object.keys(value).length === 0) || (typeof value === 'string' && value.trim().length === 0);

export const compareObjects = (firstVal, secondVal) => JSON.stringify(firstVal) === JSON.stringify(secondVal);

export const formatDate = (date) => {
  if (!date) return '';
  const dateString = date.slice(0, 10).split('-');
  return `${dateString[0]}-${dateString[1]}-${dateString[2]}`;
};

export const dateToLocalTimezone = (date, revert) => {
  Date.prototype.addHours = function (h) {
    this.setTime(this.getTime() + h * 60 * 60 * 1000);
    return this;
  };
  let offset = new Date().getTimezoneOffset() / 60;
  offset = revert ? offset : -1 * offset;
  date = date.addHours(offset);
  return date;
};
export const formatAMPM = (date) => {
  let hours = date.getHours();
  let minutes = date.getMinutes();
  let ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes.toString().padStart(2, '0');
  let strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
};

export const parserObject = (a, r) =>
  a.map((o) =>
    Object.keys(o)
      .map((key) => ({ [r[key] || key]: o[key] }))
      .reduce((a, b) => Object.assign({}, a, b))
  );

export const TrimText = (text, max_length) => {
  return !!text && text.length > max_length ? `${text.slice(0, max_length - 2)}..` : text;
};

export const isValidURL = (url) => {
  const pattern = new RegExp(
    '^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + //port
      '(\\?[;&amp;a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$',
    'i'
  );

  return pattern.test(url);
};

export const convertTime24To12 = (time) => {
  var timeParts = time.split(':');
  var AmOrPm = timeParts[0] >= 12 ? 'pm' : 'am';
  var hours = timeParts[0] % 12 || 12;
  var minutes = timeParts[1];
  return hours + ':' + minutes + ' ' + AmOrPm;
};

export const getAttrData = (dataAttr, data) => {
  if (dataAttr.length > 0)
    dataAttr.map((attr, index) => {
      if (index == 0) dataTemp = data[attr];
      else dataTemp = dataTemp[attr];
    });
  return dataTemp;
};
