import { isEmpty } from './Helper';

export const beautifyQueries = (queries, use_prefix = true) => {
  var requestQueries = '';
  if (!!queries && Object.entries(queries).length > 0)
    Object.entries(queries).map((query) => {
      if (!isEmpty(query[1]))
        if (requestQueries == '' && use_prefix == true) requestQueries = `?${query[0]}=${query[1]}`;
        else requestQueries += `&&${query[0]}=${query[1]}`;
    });
  return requestQueries;
};

export const beautifyParams = (params) => {
  var requestParams = '';
  if (!!params && Object.entries(params).length > 0)
    Object.entries(params).map((param) => {
      requestParams += `/${param[1]}`;
    });
  return requestParams;
};
