var domain = 'example.com';

export default {
  pubnub: {
    subscribeKey: 'sub-c-37c7193e-8432-11ec-9f2b-a2cedba671e8',
    publishKey: 'pub-c-a1e5f84b-74b1-464b-8964-c12f492bb292',
  },
  notification: {
    senderId: 172597554333,
  },
  version: {
    android: '1.0.0',
    ios: '1.0.0',
  },
  url: {
    api: `https://${domain}/api/`,
    root: `https://${domain}/`,
    socket: `https://${domain}`,
  },
  api: {
    timeout: 100 * 1000,
    clientToken: 'SECRET_TOKEN',
    version: '1.0.0',
  },
  headers: {
    'Content-Type': 'application/json',
  },
  storageKey: {
    token: 'APP_TOKEN',
    language: 'APP_LANG',
  },
};
