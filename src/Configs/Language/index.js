import en from './en';
import ar from './ar';

export default {
  storageKey: 'PCR_LANG',
  resources: { en, ar },
  default: 'en',
  langs: [
    { label: 'English', value: 'en', dir: 'ltr' },
    { label: 'French', value: 'fr', dir: 'ltr' },
    { label: 'العربية', value: 'ar', dir: 'rtl' },
  ],
};
