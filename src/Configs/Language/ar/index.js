import homePage from './homePage';
import common from './common';
import validation from './validation';

export default {
  homePage,
  common,
  validation,
};
