export default {
  requiredField: 'arabic.',
  maxLength: 'Maximum length is ',
  minLength: 'Minimum length is ',
  exactLength: 'Field length should be ',
  onlyNumber: 'Only numbers is valid.',
  notMatched: 'Not matched',
  invalidEmail: 'Invalid email address.',
};
