export default {
  requiredField: 'This field is required.',
  maxLength: 'Maximum length is ',
  minLength: 'Minimum length is ',
  exactLength: 'Field length should be ',
  onlyNumber: 'Only numbers is valid.',
  notMatched: 'Not matched',
  invalidEmail: 'Invalid email address.',
};
