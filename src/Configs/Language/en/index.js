import homePage from './homePage';
import validation from './validation';
import common from './common';

export default {
  homePage,
  common,
  validation,
};
