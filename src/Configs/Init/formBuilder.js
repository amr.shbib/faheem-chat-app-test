// import {Component } from '~/Components/Custom';
import { setFormBuilderConfig } from '~/Libs/Form';

export default () => {
  const formConfig = {
    fields: {
      // Component
    },
    validatorsMessage: {
      isRequired: 'This field is required',
      minLength: 'Min length is: ',
      maxLength: 'Max length is',
      exactLength: 'cc',
      isMatches: 'dd',
      isEmailAddress: 'Invalid Email',
      isMobileNumber: 'Invalid phone number',
      isNumber: 'This field must be a number',
    },
  };
  setFormBuilderConfig(formConfig);
};
