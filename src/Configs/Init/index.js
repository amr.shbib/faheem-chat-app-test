import PubNub from 'pubnub';
import { initNotification } from '~/Services/Notifications';
import Constants from '~/Configs/Constants';
import formBuilder from './formBuilder';
import navigationBuilder from './navigationBuilder';

const {
  pubnub: { subscribeKey, publishKey },
} = Constants;

export default ({ pubnub }) => {
  formBuilder();
  navigationBuilder();
  initNotification(pubnub);
};
