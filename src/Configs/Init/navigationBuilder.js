import { darkTheme } from '~/Constants/Themes';
import { setNavigationConfig } from '~/Libs/Navigation';
import { Conversation, Home, Startup } from '~/Screens';

const navigationBuilder = () => {
  const App = [
    {
      type: 'Stack',
      dependencies: [{ prop: 'startup.loading', isValid: (value) => value == true }],
      screens: [
        {
          name: 'Startup',
          component: Startup,
          options: { headerShown: false },
        },
      ],
    },
    {
      type: 'Stack',
      dependencies: [{ prop: 'account.userData.id', isValid: (value) => !!!value }],
      screens: [{ name: 'Home', component: Home, options: { headerShown: false } }],
    },
    {
      type: 'Stack',
      dependencies: [{ prop: 'account.userData.id', isValid: (value) => !!value }],
      screens: [{ name: 'Conversation', component: Conversation, options: { headerStyle: { backgroundColor: darkTheme.palette.secondary } } }],
    },
  ];
  const navigationConfig = { navigations: App };
  setNavigationConfig(navigationConfig);
};

export default navigationBuilder;
